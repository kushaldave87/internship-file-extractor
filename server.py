from encroachmentfile import EncroachmentFile
import sqlite3 as lite
import binascii

#does this commit properly

class Server:
    def __init__(self, enc_file):
        self.enc_file = enc_file

    def getDatabaseConnection(self):

        db_path = r'/Users/kush/downloads/VisualCode/ExtractingEncroachmentPermits/ENC.db'
        conn = lite.connect(db_path)

        return conn

    def bin_insert(self, conn=None, enc_file=None):
        with conn:
            cursor = conn.cursor()

            # build up SQL commands

            sql_str = "INSERT INTO PROJECTS_POINTS (OBJECTID, \
            PROJECTNAME, \
            PROJECTSTATUS, \
            PROJECTNUMBER, \
            PROJECTSTARTDATE, \
            PROJECTENDDATE, \
            PROJECTDESCRIPTION, \
            CITYCONTACT, \
            CITYPHONE, \
            CITYEMAIL, \
            REQUESTER, \
            SHAPE, \
            DEVELOPERCONTACT, \
            DEVELOPERPHONE, \
            DEVELOPEREMAIL, \
            PROJECTTYPE, \
            SYMBOLOGY) VALUES ('{oid}', '{project_name}', '{project_status}', \
            '{project_number}',\
            '{project_startdate}', \
            '{project_enddate}',\
            '{project_description}',\
            '{city_contact}',\
            '{city_phone}',\
            '{city_email}',\
            '{requester}',\
            '{shape}',\
            '{developer_contact}',\
            '{developer_phone}',\
            '{developer_email}',\
            '{project_type}',\
            '{symbology}')".format(
                oid = enc_file.ObjectID,
                project_name = enc_file.ProjectName,
                project_status = enc_file.ProjectStatus,
                project_number = enc_file.ProjectNumber,
                project_startdate = enc_file.ProjectStartDate,
                project_enddate = enc_file.ProjectEndDate,
                project_description = enc_file.ProjectDescription,
                city_contact = enc_file.CityContact,
                city_phone = enc_file.CityPhone,
                city_email = enc_file.CityEmail,
                requester = enc_file.Requester,
                shape = enc_file.Shape,
                developer_contact = enc_file.DeveloperContact,
                developer_phone = enc_file.DeveloperPhone,
                developer_email = enc_file.DeveloperEmail,
                project_type = enc_file.ProjectType,
                symbology = enc_file.Symbology
                
            )
            cursor.execute(sql_str)
            conn.commit()
            cursor.close()
    def main(self):
        enc_file = self.enc_file.enc_file
        filenames = []

        filenames.append(enc_file)

        # print datetime.now().strftime('%H:%M:%S')
        conn = self.getDatabaseConnection()
        for enc_file in filenames:
            self.bin_insert(conn, enc_file)
        conn.close()
