from pdfextractor import PDFExtraction
from server import Server
import os
import re
import sqlite3

folder_path = r'/Users/kush/Downloads/VisualCode/ExtractingEncroachmentPermits/EncroachmentPDFs'  # replace with the path to your folder
pdf_files = [f for f in os.listdir(folder_path) if f.endswith('.pdf')]
for i in range(len(pdf_files)):
    pdf_file = pdf_files[i]

    pdf_file_list = []
    pdf_file_letters = [*pdf_file]
    for i in range(len(pdf_file_letters)):
        if pdf_file_letters[i] != ".":
            pdf_file_list.append(pdf_file_letters[i])
        else:
            break
    project_name = " ".join(pdf_file_list)
    print(project_name)

    # Connect to SQLite database
    db_path = r'/Users/kush/downloads/VisualCode/ExtractingEncroachmentPermits/ENC.db'
    conn = sqlite3.connect(db_path)
    # Create a cursor object to execute SQL commands
    cur = conn.cursor()

    cur.execute('SELECT * FROM PROJECTS_POINTS WHERE PROJECTNUMBER = ?', (project_name,))

    # Fetch the results
    results = cur.fetchall()

    # Check if the value already exists in the table
    if len(results) > 0:
        file_in_database = False
    else:
        file_in_database = True

        # Commit changes to the database
        conn.commit()

    # Close the connection
    conn.close()

    if file_in_database:
        populated_enc_file = PDFExtraction()
        populated_enc_file.extractingthepdf(pdf_file)

        connectingtoserver = Server(populated_enc_file)
        connectingtoserver.main()
        
        print("done")
    else:
        print("File Already In Database")