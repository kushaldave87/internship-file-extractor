import pytesseract
from PIL import Image, ImageEnhance
from PIL import ImageFilter
import re
import fitz
from encroachmentfile import EncroachmentFile
import sqlite3 as lite

#does this commit properly

class PDFExtraction():
    def __init__(self):
        self.enc_file = EncroachmentFile

    def getaddresscoord(self, address):
        coord = " "
        return coord

    # Open the scanned document as an image
    # Open the PDF file
    def extractingthepdf(self, pdf_file):
        folder_path = r'/Users/kush/Downloads/VisualCode/ExtractingEncroachmentPermits/EncroachmentPDFs/'
        pdf_path = folder_path + pdf_file
        
        # pdf_path = '/Users/kush/Downloads/EP 455-22.pdf'
        nums = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        pdf_file_list = []
        pdf_file_letters = [*pdf_file]
        for i in range(len(pdf_file_letters)):
            if pdf_file_letters[i] != ".":
                pdf_file_list.append(pdf_file_letters[i])
            else:
                break
        project_name = " ".join(pdf_file_list)
        # print(project_name)

        with fitz.open(pdf_path) as doc:

            # Get the first page of the document
            page = doc[0]

            # Render the page as an image with a higher resolution
            zoom_x = 2.0  # horizontal zoom
            zoom_y = 2.0  # vertical zoom
            mat = fitz.Matrix(zoom_x, zoom_y)
            pix = page.get_pixmap(matrix=mat)

            # Convert the pixmap to a PIL image
            img = Image.frombytes("RGB", [pix.width, pix.height], pix.samples)

            # Save the image as a PNG file
            img.save('intern_test_image_2.png')
        #Opens the image
        image = Image.open('intern_test_image_2.png')

        # /Users/kush/Downloads/EP 455-22-1
        # /Users/kush/Downloads/internshipEPdoc-1
        # San Ram DIVISION encroachmentpermit@sanramon.ca.gov Encroachment Permit
        # San Ramon DIVISION encroachmentpermit@sanramon.ca.gov Encroachment Permit
        # output_image
        # Convert the image to grayscale mode
        image = image.convert('L')

        # Enhance the contrast of the image
        enhancer = ImageEnhance.Contrast(image)
        image = enhancer.enhance(2) # increase contrast by a factor of 2
        # Use pytesseract to extract text from the image

        text = pytesseract.image_to_string(image).split('\n')

        list_to_str = " ".join(text) #Makes it so each item in the list is a word, not multiple words
        organized_list = list_to_str.split(" ")

        data = []
        final_list = []

        nums = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

        for item in organized_list: #Takes out all the spaces in the list
            if item == '':
                continue
            else:
                data.append(item)

        for i in range(len(data)): #deletes all unnecessary information
            if data[i] != "HEREBY":
                final_list.append(data[i])
            else:
                del final_list[-1]
                break


        for i in range(len(final_list)):
            if final_list[i] == "|":
                if final_list[i+1] == "San":
                    permit_num = final_list[i-1]
                else:
                    permit_num = final_list[i+1]
                break


        date_list = []

        for item in final_list:
            import re
            match = re.search(r'\d{1,2}/\d{1,2}/\d{2,4}', item)  # search for a date pattern
            if match:
                start_date_final = match.group()  # extract the matched date
                break

        for i in range(len(final_list)):
            if final_list[i] == "ENGINEERING":
                end_date = final_list[i-1]
                end_date_list = [*end_date]
                break

        if end_date_list[0] not in nums:
            del end_date_list[0]
        if end_date_list[-1] not in nums:
            del end_date_list[-1]

        end_date_final = "".join(end_date_list)






        applicant_name_list = []

        for i in range(len(final_list)): #gets the applicant name
            if final_list[i] == "FAX":
                k = i + 1
                while True:
                    applicant_name_list.append(final_list[k])
                    k += 1
                    if final_list[k] == "ADDRESS:" or final_list[k] == '‘ADDRESS:':
                        break

        last_word = applicant_name_list[-1]
        applicant_final_list = []
        applicant_letters = [*last_word]

        for character in applicant_letters:
                try:
                    character = int(character)

                except:
                    TypeError
                
                if type(character) == int:
                    del applicant_name_list[-1]
                    break

                    
                else:
                    continue

        applicant_name = " ".join(applicant_name_list)


        address_list = []
        for i in range(len(final_list)): #This whole thing finds the address
            if final_list[i] == "WORK":
                x = i-5
                while True:
                    address_list.append(final_list[x])
                    x -=1
                    if final_list[x] == "COMPLETION":
                        break


                    

        address_list.reverse()  #this makes the address look organized and propper
        address_list.remove(address_list[0])
        new_address_list = []
        for i in range(len(address_list)):
            if address_list[i] != "DATE":
                new_address_list.append(address_list[i])

            else:
                break

        address = " ".join(new_address_list)




        for i in range(len(final_list)): #finds the "Description of work" category and gets respective information
            if final_list[i] != "):":
                continue
            else:
                description_of_work_list = final_list[i:]
                del description_of_work_list[0]
                break

        final_description_list = []

        for item in description_of_work_list:
            letters = [*item]
            for i in range(len(letters)):
                if (letters[i] == "'" or letters[i] == "’") and letters[i-1] in nums:
                    letters[i] = "ft "
                if letters[i] == '"' and letters[i-1] in nums:
                    letters[i] = "in "
            new_word = "".join(letters)
            final_description_list.append(new_word)

        description_of_work = " ".join(final_description_list)

        new_person = []
        person = []
        for i in range(len(final_list)):
            if final_list[i] == "LOCATION(S)":
                h = i - 1
                while final_list[h] != "PHONE":
                    person.append(final_list[h])
                    h = h-1

        person.reverse()

        final_person_list = []

        for item in person:
            each_letter = [*item]
            for character in each_letter:
                try:
                    character = int(character)

                except:
                    TypeError
                
                if type(character) == int:
                    break
                else:
                    final_person_list.append(character)

            final_person_list.append(" ")

        final_person = "".join(final_person_list)

        import re

        x = final_list.index("Permit")
        find_phone_list = final_list[x: ]


        phone_num_string = " ".join(find_phone_list)

        # define a regular expression pattern for phone numbers
        pattern = re.compile(r'\d{3}[-.]?\d{3}[-.]?\d{4}')


        # find all phone numbers in the text using the regular expression pattern
        phone_numbers = re.findall(pattern, phone_num_string)
        contact_number = phone_numbers[0]


        enc_file = EncroachmentFile()
        db_path = r'/Users/kush/downloads/VisualCode/ExtractingEncroachmentPermits/ENC.db'
        conn = lite.connect(db_path)

        # Create a cursor object to execute SQL commands
        cur = conn.cursor()
        conn.commit()

        # Execute SQL query to get the max value of "ObjectID"
        cur.execute('SELECT MAX(OBJECTID) FROM PROJECTS_POINTS')

        # Fetch the result
        max_object_id = cur.fetchone()[0] #This is the max object ID

        
        # Close the database connection
        conn.commit()
        conn.close()

        enc_file.ObjectID = str((int(max_object_id) + 1))
        enc_file.ProjectName = applicant_name + " " + project_name
        enc_file.ProjectNumber = project_name
        enc_file.ProjectStatus = "Current"
        enc_file.ProjectStartDate = str(start_date_final)
        enc_file.ProjectEndDate = str(end_date_final)
        enc_file.ProjectDescription = str(description_of_work)
        enc_file.ProjectType = "ENC"
        enc_file.CityContact = "Gordon Despins"
        enc_file.CityPhone = "9259732693"
        enc_file.CityEmail = "gdespins@sanramon.ca.gov"
        enc_file.Requester = str(applicant_name)
        enc_file.Shape = self.getaddresscoord(address)
        enc_file.DeveloperContact = str(final_person)
        enc_file.DeveloperPhone = str(contact_number)
        enc_file.DeveloperEmail = ""
        enc_file.Symbology = ""
        self.enc_file = enc_file
